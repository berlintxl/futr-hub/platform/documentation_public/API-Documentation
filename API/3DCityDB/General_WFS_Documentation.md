# General WFS Documentation

The 3D City DB WFS is provided as Open Source Software [here](https://www.3dcitydb.org/3dcitydb/). The maintainers provide a comprehensive documentation. We link here to the most important topics:

* [General Information](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/index.html)
* [Method Overview](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/index.html)

The methods are documented in detail on the following pages. 

* [GetCapabilities](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/get-capabilities.html#wfs-getcapabilities-operation-chapter)
* [DescribeFeatureType](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/describe-feature-type.html)
* [GetFeature](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/get-feature.html)
* [GetPropertyValue](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/get-property-value.html)
* [ListStoredQueries](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/list-stored-queries.html)
* [DescribeStoredQuery](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/describe-stored-query.html)
* [CreateStoredQuery](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/create-stored-query.html)
* [DropStoredQuery](https://3dcitydb-docs.readthedocs.io/en/latest/wfs/functionality/drop-stored-query.html)