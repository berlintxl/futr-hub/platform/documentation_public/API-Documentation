# 3D City DB - WFS API

This API provides an WFS Service to the 3D CIty DB via the Gravitee gateway endpoint `/gateway/geodata-citydb-wfs`.

## Getting started

To utilize an API via the Gravitee gateway, you need an accunt registered on our Keycloak IDM, and an administrator needs to grant you access to a workspace.

The Login is described in this [documentation](https://apim.futr-hub.de/pages/476cbf00-924a-440d-acbf-00924a740d9e#api-access).

