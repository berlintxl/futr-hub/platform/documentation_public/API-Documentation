# 3D City DB - WFS API

This API provides an WFS Service to the 3D CIty DB via the APISIX endpoint `/geodata-citydb-wfs`.

## Getting started

To utilize an API via APISIX, you need an accunt registered on our Keycloak IDM, and an administrator needs to grant you access to a workspace.

The Login is described in this [documentation](../api-access-documentation.MD).

