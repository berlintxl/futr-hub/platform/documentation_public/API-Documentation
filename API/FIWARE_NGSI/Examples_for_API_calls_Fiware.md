## Examples for API calls

### Wärmemengenzähler

The basic URL /entities with the Method GET is able to ready any sensor or entity values from the platform. According to your rights, yo have to specify at least one parameter, to get the rights ones:

The Header `Fiware-Service` specifies the dataspace, which is used to read the entities. In the example collection, this is set to `waermemengenzaehler`. So make sure, that this is also specified in any other client

A second important part is, that the /entities GET Method supports paging. The two Queryparameters `offset` and `limit` handle the standard paging. If you do not specify these, the `offset` will be 0 and the `limit` will be 100. So you get the first 100 values. To get more, you have to change the parameters and send more than one request. 

See Screenshot:
![](waermengenzaehler_GET.png)


### Wetterdaten (DWD) 

The basic URL /entities with the Method GET is able to ready any sensor or entity values from the platform. According to your rights, yo have to specify at least one parameter, to get the rights ones:

The Header `Fiware-Service` specifies the dataspace, which is used to read the entities. In the example collection, this is set to `ds_nda_kachelmann`. So make sure, that this is also specified in any other client

A second important part is, that the /entities GET Method supports paging. The two Queryparameters `offset` and `limit` handle the standard paging. If you do not specify these, the `offset` will be 0 and the `limit` will be 100. So you get the first 100 values. To get more, you have to change the parameters and send more than one request. 

See Screenshot:
![](Screenshot_Kachelmann_GetRequest.png)
