# FIWARE NGSI LD API
This API provides an interface to the Stellio Context Broker via the APISIX endpoint `/stellio/api` .

## Getting started
In order to use an API via APISIX, you need to have an account created on our keycloak IDM and have an adminstrator grant you access to a dataspace.

For Accessing the API with valid credentials, please see [API Access](../api-access-documentation.MD)

### Data Spaces
This API supports a generic approach to tenancy, by seperating data in so-called data spaces. These spaces are reflected by `tenants` and can only be accessed, if your keycloak-user is in the related group and you have been granted a role to read/write/administer data in this space. To specify the `tenant` in a request, the header `NGSILD-Tenant` can be used. If no `tenant` is specified, the default `tenant` will be used. A `tenant` name starts with `urn:ngsi-ld:tenant:` followed by the name of the `tenant`. 

More on the concept of tenants can be read here: [Stellio Documentation](https://stellio.readthedocs.io/en/latest/user/multitenancy.html)

### Addtional Documentation
For more information on the Stellio API, please see [the official documentation](https://stellio.readthedocs.io/en/latest/API_walkthrough.html). This will cover the API in more detail and also explain how historical data can be accessed and how subscriptions can be created.

For information on NGSI LD in general, please see the [NGSI LD Documentation](https://ngsi-ld-tutorials.readthedocs.io/en/latest/). This contains tutorials and examples, which can help explain the concepts behind NGSI LD.
