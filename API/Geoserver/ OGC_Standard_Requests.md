## Guide to Operations in Open Geospatial Consortium (OGC) Web Services and Tile Map Service

The official website provides multiple guides, best practices, and maintains git repositories with use cases and data models.

* [WFS_Operations](http://opengeospatial.github.io/e-learning/wfs/text/operations.html)
* [WMS_Operations](http://opengeospatial.github.io/e-learning/wms/text/operations.html)
* [WMTS_Operations](https://opengeospatial.github.io/e-learning/wmts/text/operations.html)
* [CSW Operations](http://opengeospatial.github.io/e-learning/cat/text/main.html#overview-of-csw-operations)
* [WCS Operations](http://opengeospatial.github.io/e-learning/wcs/text/operations.html)
* [WPS Operations](http://opengeospatial.github.io/e-learning/wps/text/operations.html)
* [TMS Documentation](https://wiki.osgeo.org/wiki/Tile_Map_Service_Specification)

## Description of the REST API Configuration

[Geoserver REST API](https://docs.geoserver.org/main/en/user/rest/api/index.html)