# Geoserver API
This API provides an interface to the Geoserver via the APISIX endpoint `/geoserver`.

## Getting started
To utilize an API via APISIX, you need an accunt registered on our Keycloak IDM, and an administrator needs to grant you access to a workspace.

For Accessing the API with valid credentials, please see [API Access to Geoserver](../api-access-documentation.MD)

## Workspaces
This API facilitates a generic approach to tenancy through the division of data into so-called dataspaces. These dataspaces are represented as Geoserver workspaces and can only be accessed if your Keycloak-user is a member of the related group and you have been granted a role to read data in this dataspace.

## Example Requests

An example API request onto the Geoserver to request all layer names in ds_opendata dataspace looks like this:

curl -X GET \
  https://geoportal.staging.futr-hub.de/geoserver/rest/workspaces/ds_opendata/featuretypes.json


With the [Thunder Client](https://www.thunderclient.com/)  an example API request onto the Geoserver  like this looks like this:

Get Request URL is:

https://geoportal.staging.futr-hub.de/geoserver/rest/workspaces/ds_opendata/featuretypes.json

 Screenshot: ![Screenshot API Request Geoserver](API_Request_Geoserver_APISIX.png)
