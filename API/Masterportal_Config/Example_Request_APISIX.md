## Example Requests

An example API request onto a Masterportal Config file looks like this:

First you need to get the bearer token:

Using this command (get the correct client_secret, username and password first)

curl -X POST \
  https://idm.staging.futr-hub.de/auth/realms/futr-hub-datenplattform/protocol/openid-connect/token \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "grant_type=password" \
  -d "client_id=api-access" \
  -d "client_secret=some_secret" \
  -d "username=some_username" \
  -d "password=your_password"

Now request with that bearer token you just received an api call like this:

 curl -X GET \
  https://api.staging.futr-hub.de/geodataconfig/services-internet.json \
  -H "Authorization: Bearer YOUR_ACCESS_TOKEN"


Get Request URL is:

https://apim.staging.futr-hub.de/gateway/geodataconfig/services-internet.json

Using [Thunder Client](https://www.thunderclient.com/) in VS Code for example gives you following result:

 Screenshot: ![Screenshot API Request Masterportal Config](API_Request_Masterportal_config_APISIX.png)