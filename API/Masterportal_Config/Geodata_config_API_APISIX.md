# Masterportal Config API

This API allows interaction with Masterportal through endpoints related to the `services-internet.json`, `rest-service.json`, and `config.json` files. 

## Getting started

In order to use the Masterportal API, it's necessary to have a user account, with the user's role defining the services they have access to.

For Accessing the API with valid credentials, please see [API Access](../api-access-documentation.MD)

## Services

The services available to the user are based on their assigned role. These services are dictated by the `services-internet.json`, `rest-service.json`, and `config.json` files. With these, a user can create an info desk, or a device with a screen running Masterportal.

## Swagger Doc

Swagger documentation is provided alongside this API description, detailing everything you need to work with the API.

[Swagger Doc Masterportal Config API](Swagger_Doc_Masterportal_Config_API.yaml)

## Further readings

You can consult the [official Masterportal documentation](https://www.masterportal.org/files/masterportal/html-doku/doc/latest/doc.html) for a comprehensive guide, best-practices, and additional resources.