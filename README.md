# Project API Documentation

## Overview

This repository contains comprehensive documentation for APIs managed by our project via Gravitee. Each API has a General Information Document, a Swagger Document, and a collection of Example Requests to assist developers in integrating our services effectively.

## Table of Contents

- [Available APIs](#available-apis)
  - [FIWARE-NGSI v2 Orion Context Broker (1.0)](#fiware-ngsi-v2-orion-context-broker-10)
  - [Masterportal Config](#masterportal-config)
  - [Geoserver (1)](#geoserver-1)
- [API Access Documentation](#api-access-documentation)

## Available APIs

### FIWARE-NGSI v2 Orion Context Broker (1.0)

- **General Information**: [Link to Document](~/API/FIWARE_NGSI/General_Usage_Doc.MD)
- **Swagger Document**: [Link to Swagger](~/API/FIWARE_NGSI/Fiware_API_SWAGGER_Doc.yml)
- **Example Requests**: [Link to Examples](~/API/FIWARE_NGSI/Examples_for_API_calls_Fiware.md)

### Masterportal Config

- **General Information**: [Link to Document](~/API/Masterportal_Config/Masterportal_Config_API.MD)
- **Swagger Document**: [Link to Swagger](~/API/Masterportal_Config/Swagger_Doc_Masterportal_Config_API.yml)
- **Example Requests**: [Link to Examples](~/API/Masterportal_Config/Example_Requests.md )

### Geoserver (1)

- **General Information**: [Link to Document](~/API/Geoserver/Geoserver_API_Documentation.MD)
- **OGC Standard Requests**: [Link to Examples](~/API/Geoserver/OGC_Standard_Requests.md)

## API Access Documentation

- **API Access Documentation**: [Link to Document](~/API/api-access-documentation.MD)

---

For more information, please contact our technical support team.